import React from 'react';
import Nav from './src/navigation/Authenticated';
import NavigationService from './src/navigation/NavigationService';
import SplashScreen from 'react-native-splash-screen'
import * as RNLocalize from "react-native-localize";
import {StyleSheet, I18nManager} from 'react-native';
import i18n from "i18n-js";
import memoize from "lodash.memoize"; // Use for caching/memoize for better performance


export const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  es: () => require("./src/translations/es.json"),
  en: () => require("./src/translations/en.json"),
};

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = { languageTag: "en", isRTL: false };

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  // clear translation cache
  translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(isRTL);
  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;
};

class App extends React.Component {
  componentDidMount() {
    // do stuff while splash screen is shown
      // After having done stuff (such as async tasks) hide the splash screen
      SplashScreen.hide();
  }
  render() {
    return (
      <Nav ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

export default App;

const styles = StyleSheet.create({
  title: {
    fontSize: 32,
    marginHorizontal: 16,
    marginVertical: 8,

  },
});
