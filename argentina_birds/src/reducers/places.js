import {GET_PLACES_SUCCESS} from '../constants'

const initialState = {
  list: [],
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PLACES_SUCCESS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

export default placesReducer;
