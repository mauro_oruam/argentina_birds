import {
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({

  viewFlexRow: {
    flexDirection: 'row',
  },
  menuButton: {
    width: 25,
    height: 25,
    marginLeft: 5,
  },
  bars: {
    marginLeft: 15,
    color: 'white',
  },
  drawerContent: {
    flex: 1,
  }

});

export default styles;
