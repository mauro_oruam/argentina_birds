import { SET_REGISTER, SET_REGISTER_SUCCESS,  GET_TOURS, GET_TOURS_SUCCESS } from '../constants';

export function setRegister (values) {
  return {
    type: SET_REGISTER,
    data: values,
  }
}

export function setRegisterSuccess (user) {
  return {
    type: SET_REGISTER_SUCCESS,
    payload: user,
  }
}


export function getTours (values) {
  return {
    type: GET_TOURS,
    data: values,

  }
}

export function getToursSuccess (tours) {
  return {
    type: GET_TOURS_SUCCESS,
    payload: tours,
  }
}
