import {
  call, put, takeLatest, take,
} from 'redux-saga/effects';

import { SET_REGISTER } from '../constants';
import { setRegisterSuccess } from '../actions/users';
import { setRegisterRequest } from '../api';
import NavigationService from '../navigation/NavigationService';

import { GET_TOURS } from '../constants';
import { getToursRequest } from '../api';
import { getToursSuccess } from '../actions/users';

function* toursWorker(values) {
  try {
    const tours = yield call(getToursRequest);

    //VER COMO ENVIAR LOS VALUES JUNTO AL PAYLOAD
    NavigationService.navigate('CreateItinerary2', {payload:{
      "tours": tours,
      "data": values
    }});

  } catch (e) {
    console.warn('toursWorker', e);
  }
}

export function* toursWatcher() {
  while (true) {
    const { data } = yield take(GET_TOURS);
    try {
      yield call(toursWorker, data);
    } catch (e) {
      console.warn('error toursWatcher:', e);
    }
  }

}

/**
 * SET_REGISTER
 */
function* setRegisterWorker(values) {
  try {
    const data = yield call(setRegisterRequest, values);
    if (data) {
      // TODO: que tenemos que pasarle al setRegisterSuccess
      yield put(setRegisterSuccess(values));
      NavigationService.navigate('PlacesList');
    }
  } catch (e) {
    console.warn('error setEnrollmentWorker:', e);
  }
}

export function* setRegisterWatcher() {
  while (true) {
    const { data } = yield take(SET_REGISTER);
    try {
      yield call(setRegisterWorker, data);
    } catch (e) {
      console.warn('error setRegisterWatcher:', e);
    }
  }
}
