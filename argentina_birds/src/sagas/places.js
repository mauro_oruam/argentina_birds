import {
  call, put, takeLatest, take,
} from 'redux-saga/effects';

import { GET_PLACES } from '../constants';
import { getPlacesSuccess } from '../actions/places';
import { getPlacesRequest } from '../api';

function* placesWorker() {
  try {
    const places = yield call(getPlacesRequest);
    yield put(getPlacesSuccess(places));
  } catch (e) {
    console.warn('EXCEPTION', e);
  }
}

export function* placesWatcher() {
  yield takeLatest(GET_PLACES, placesWorker);
}
