import { all } from 'redux-saga/effects';
import { placesWatcher } from './places';
import { setRegisterWatcher, toursWatcher } from './users';


export default function* rootSaga() {
  yield all([
    placesWatcher(),
    setRegisterWatcher(),
    toursWatcher(),
  ]);
}
