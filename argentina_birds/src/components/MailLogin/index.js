import React from 'react';
import {
  ScrollView, Text, View, StyleSheet
} from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import { Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import NavigationService from '../../navigation/NavigationService';

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .label('Email')
    .email("Insert a valid email address")
    .required("Required field"),
  password: yup
    .string()
    .label('Password')
    .required("Required field"),
});


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {};
  }

  onSubmit = (values) => {
    console.warn(values)
  }

  focusNextField = (key) => {
    this.fields[key].focus();
  }

  render() {

    return (
      <Formik
        initialValues={{ email: '', password: '', id: 'frs' }}
        validationSchema={validationSchema}
        onSubmit={this.onSubmit}
      >
        {({
          values, handleChange, isValid, submitForm, errors, touched, handleBlur, handleSubmit
        }) => (
          <ScrollView>
            <Text style={styles.presentation}>Login</Text>
            <Input
              label="E-mail"
              value={values.email}
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              placeholder="E-mail"
              labelStyle={styles.inputslabel}
              keyboardType="email-address"
              name= 'email'
              valid={touched.email && !errors.email}
              error={touched.email && errors.email}
              returnKeyType="next"
              autoCapitalize="none"
              leftIcon={(
                <Icon
                  name="envelope"
                  size={12}
                  color="grey"
                />
              )}
              ref={(input) => {
                this.fields.mail = input;
              }}
              onSubmitEditing={() => {
                this.focusNextField('password');
              }}
              blurOnSubmit={false}
            />
            {errors.email && touched.email ? (
                <Text style={styles.formError}>{errors.email}</Text>
            ) : null}
            <Input
              label="Contraseña"
              value={values.password}
              onChangeText={handleChange('password')}
              placeholder="Contraseña"
              secureTextEntry
              name= 'password'
              valid={touched.password && !errors.password}
              error={touched.password && errors.password}
              labelStyle={styles.inputslabel}
              leftIcon={(
                <Icon
                  name="key"
                  size={12}
                  color="grey"
                />
              )}
              ref={(input) => {
                this.fields.password = input;
              }}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                submitForm();
              }}
              returnKeyType="done"
            />
            {errors.password && touched.password ? (
                <Text style={styles.formError}>{errors.password}</Text>
            ) : null}
            <Button
              title="Enviar"
              type="outline"
              disabled={!isValid}
              onPress={handleSubmit}
              buttonStyle={styles.submitButton}
            />

          </ScrollView>
        )}
      </Formik>
    );
  }
}

export default Login;

// Later on in your styles..
const styles = StyleSheet.create({
  formError:{
    color: 'red',
    marginHorizontal: 16,
  },
  submitButton:{},
  inputslabel:{},
  presentation:{
    marginHorizontal: 16,
    fontSize: 20,
    fontWeight: 'bold',
    paddingVertical: 20
  },


});
