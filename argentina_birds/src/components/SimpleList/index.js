import React from 'react';
import {
  Text,
  View,
  FlatList,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';

import { getPlaces } from '../../actions/places';

class SimpleList extends React.Component {

  componentDidMount = () => {
    const { loadPlaces } = this.props;
    loadPlaces();
  }

  render() {
    const { data } = this.props;

    return (

        <FlatList
          data={data}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
              <View>
                <Text>{item.title}</Text>
              </View>
          )}
        />
    );
  }
}

const mapStateToProps = (state) => ({
  data: state.places.list,
});

const mapDispatchToProps = (dispatch) => ({
  loadPlaces: () => dispatch(getPlaces()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SimpleList);
