import React, {Component} from 'react';
import {View, Button, Platform, Text} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

export default class App extends Component {
  state = {
    date: new Date(),
    dateEnd: new Date(),
    mode: 'date',
    show: false,
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
    });
  }

  setDateEnd = (event, dateEnd) => {
    dateEnd = dateEnd || this.state.dateEnd;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      dateEnd,
    });
  }

  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }

  datepicker = () => {
    this.show('date');
  }



  render() {
    const { show, date, dateEnd, mode } = this.state;
    let month = date.getMonth() + 1
    let day = date.getDate()
    let year = date.getFullYear()
    let monthEnd = dateEnd.getMonth() + 1
    let dayEnd = dateEnd.getDate()
    let yearEnd = dateEnd.getFullYear()

    return (
      <View>
        <View>
          <Button onPress={this.datepicker} title={month + ' ' + day + ' ' + year}/>
        </View>
        { show && <DateTimePicker value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDate} />
        }
        <View>
          <Button onPress={this.datepicker} title={monthEnd + ' ' + dayEnd + ' ' + yearEnd} />
        </View>
        { show && <DateTimePicker value={dateEnd}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDateEnd} />
        }
      </View>
    );
  }
}
