import React from 'react';
import { View, Text, FlatList } from 'react-native';
import NavigationService from '../../navigation/NavigationService'

class Generic extends React.Component  {

  render () {
    const {navigation} = this.props;
    const items = navigation.state.params.payload
    return (
      <FlatList
        data={items}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
            <View>


            { item.turno == 'M' && (
              <Text style={{color: 'red'}}>{item.name}</Text>
            )}
            { item.turno == 'T' && (
              <Text style={{color: 'blue'}}>{item.name}</Text>
            )}
            { item.turno == 'N' && (
              <Text style={{color: 'green'}}>{item.name}</Text>
            )}

            </View>


        )}
      />

    );
  }
}

export default Generic;
