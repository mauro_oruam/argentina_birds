import React from 'react';
import { View, Platform, StyleSheet, Text, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button } from 'react-native-elements'
import NavigationService from '../../navigation/NavigationService'
import DateTimePicker from '@react-native-community/datetimepicker';
import * as yup from 'yup';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import Selector from '../Select';
import { getTours } from '../../actions/users';


const validationSchema = yup.object().shape({
  name: yup
  .string()
  .label('Name')
  .required("Required field"),
});

class CreateItinerary extends React.Component  {

  constructor(props) {
    super(props);
    this.fields = {};


    this.state = {
      date: new Date(),
      dateEnd: new Date(),
      mode: 'date',
      mode2: 'date',
      show: false,
      show2: false,
    }
  }

  onSubmit = (values) => {
    values.dateEnd = this.state.dateEnd
    values.date = this.state.date
    const { getTours } = this.props;
    getTours(values)
  }


  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
    });
  }

  setDateEnd = (event, dateEnd) => {
    dateEnd = dateEnd || this.state.dateEnd;
    this.setState({
      show2: Platform.OS === 'ios' ? true : false,
      dateEnd,
    });
  }

  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }
  show2 = mode2 => {
    this.setState({
      show2: true,
      mode2,
    });
  }

  datepicker = () => {
    this.show('date');
  }
  datepicker2 = () => {
    this.show2('date');
  }



  render () {
    const { show, show2, date, dateEnd, mode, mode2 } = this.state;
    let month = date.getMonth() + 1
    let day = date.getDate()
    let year = date.getFullYear()
    let monthEnd = dateEnd.getMonth() + 1
    let dayEnd = dateEnd.getDate()
    let yearEnd = dateEnd.getFullYear()
    const title = this.props.navigation.state.params.item;
    const items = [
      { id: 'Long', name: 'Long birding tours' },
      { id: 'Short', name: 'Short birding tours' },
      { id: 'Combination', name: 'Combination of birding and cultural tours' },
      { id: 'photograpy', name: 'Birds and wildlife photograpy tours' },
      { id: 'Others', name: 'Others' },
      { id: 'River', name: 'River' },
    ];

    return (
      <Formik
        initialValues={{ name:  title + " " + year + "-" + month + "-" + day, date: this.state.date, dateEnd: this.state.dateEnd}}
        validationSchema={validationSchema}
        onSubmit={this.onSubmit}
      >
        {({
          values, handleChange, isValid, submitForm,  handleBlur, handleSubmit, setFieldValue
        }) => (
    <ScrollView style={styles.container}>

    <Input
      label="Name"
      value={values.name}
      onChangeText={handleChange('name')}
      onBlur={handleBlur('name')}
      keyboardType="default"
      containerStyle={styles.containerInput}
      inputContainerStyle={styles.formInputText}
      placeholder={"Name: " + title + " " + year + "-" + month + "-" + day}
      blurOnSubmit={false}
    />


      <View style={styles.dateButtonsContainer}>
        <Button
        buttonStyle= {styles.buttonDates}
        onPress={this.datepicker}
        title={'from: ' + month + '-' + day + '-' + year}
        />
        { show && <DateTimePicker value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDate} />
        }
        <Button
        buttonStyle= {styles.buttonDates}
        onPress={this.datepicker2}
        title={'to: ' + monthEnd + '-' + dayEnd + '-' + yearEnd}
        />
        { show2 && <DateTimePicker value={dateEnd}
                    mode={mode2}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDateEnd} />
        }
    </View>
    <Selector items={items} label="Categories" setFieldValue={setFieldValue}/>


    <Button
      title="Enviar"
      type="outline"
      disabled={!isValid}
      onPress={handleSubmit}
      buttonStyle={styles.submitButton}
    />



    </ScrollView>
  )}
    </Formik>

    );
  }
}

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
  getTours: (values) => dispatch(getTours(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateItinerary);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  formInputText: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  containerInput: {
    marginTop: 10,
    padding: 20,
    paddingHorizontal:15,
  },
  dateButtonsContainer: {
    alignContent: 'space-between',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  nextButton: {
    backgroundColor: 'darkblue',
  },
  NextButtonContainer: {
    //position: 'absolute',
    //bottom: 0,
  },
  buttonDates: {
    width: 150,
    marginBottom: 50,
    //position: 'absolute',
    //bottom: 0,
  }
});
