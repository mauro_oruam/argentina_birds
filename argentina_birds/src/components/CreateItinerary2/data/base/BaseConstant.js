export const TEST_DATA = [
    {icon: require('../../data/img/animal1.png'),txt: 1},
    {icon: require('../../data/img/animal2.png'),txt: 2},
    {icon: require('../../data/img/animal3.png'),txt: 3},
    {icon: require('../../data/img/animal4.png'),txt: 4},


]

export const TXT = "Never give up, Never lose hope. \n" +
    "\n" +
    "Always have faith, It allows you to cope. \n" +
    "\n" +
    "Trying times will pass, As they always do. \n" +
    "\n" +
    "Just have patience, Your dreams will come true. \n" +
    "\n" +
    "So put on a smile, You'll live through your pain. \n" +
    "\n" +
    "Know it will pass, And strength you will gain."
