import React, {Component} from 'react';
import NavigationService from '../../navigation/NavigationService'
import { FlatGrid } from 'react-native-super-grid';
import {Dimensions, Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import DragSortableView from './widget/DragSortableView';
import {TEST_DATA} from './data/base/BaseConstant';
const {width} = Dimensions.get('window')
import {YellowBox} from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MapExample from '../MapExample'
import { withNavigationFocus } from 'react-navigation';
YellowBox.ignoreWarnings(['VirtualizedLists should never be nested'])

var moment = require('moment')

const parentWidth = width - 24

const childrenWidth = width - 24
const childrenHeight = 140

class NewDragableExample extends Component{

    constructor(props) {
        super(props)
        const {navigation} = this.props;
        const date= moment(navigation.state.params.payload.data.date, 'YYYY/MM/DD')
        const dateEnd= moment(navigation.state.params.payload.data.dateEnd, 'YYYY/MM/DD')
        const itineraryLength = dateEnd.diff(date,'days')
        const items = navigation.state.params.payload.tours.selfGuided

        const selectedMorningTours = items.filter(obj => {
          return obj.shift === 'M'
        })
        const selectedAfternoonTours = items.filter(obj => {
          return obj.shift == "A"
        })

        const selectedNightTours = items.filter(obj => {
          return obj.shift === 'N'
        })

        let days = {
          "daysSelected":[]
        }

        for (let i=0; i <= itineraryLength; i++) {
          days.daysSelected.push(
            {
              "day": 'DAY ' + (i+1),
              //"date": date + i
            }
          )}


        this.state = {
          navigation: navigation,
          data: TEST_DATA,
          scrollEnabled: true,
          items: {tours: navigation.state.params.payload.tours.selfGuided},
          selectedMorningTours: selectedMorningTours,
          selectedAfternoonTours: selectedAfternoonTours,
          selectedNightTours: selectedNightTours,
          itineraryLength: itineraryLength,
          days: days,
        }
      }

      componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
          let value = this.props.navigation.state.params.newTour
          if (value != null){
            this.setState(previousState => ({
              selectedMorningTours: [...previousState.selectedMorningTours, value]
            }));
          // Call any action
        }
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }



  removeTour = (item) => {
    if (item.shift == "M") {
      this.setState({selectedMorningTours: this.state.selectedMorningTours.filter(obj => {
        return obj.name !== item.name})})
      }
    else if (item.shift == 'A') {
      this.setState({selectedAfternoonTours: this.state.selectedAfternoonTours.filter(obj => {
        return obj.name !== item.name})})
    }else {
      this.setState({selectedNightTours: this.state.selectedNightTours.filter(obj => {
        return obj.name !== item.name})})
    }
  }


  render() {
      return (

        <ScrollView style={styles.mainContainer} scrollEnabled = {this.state.scrollEnabled}>
        <View style={styles.header}>
            <FlatGrid
              scrollEnabled={false}
              itemDimension={50}
              items={this.state.days.daysSelected}
              style={styles.gridView}
              renderItem={({ item, index }) => (
                <TouchableOpacity style={styles.daysButton} onPress={() =>
                  console.warn(days.daysSelected[index])}>
                  <Text> {item.day} </Text>
                </TouchableOpacity>
              )}
            />
          <TouchableOpacity>
            <FontAwesome5 style={styles.trash} name="trash-alt" solid size={20} />
          </TouchableOpacity>
          <TouchableOpacity>
            <FontAwesome5 style={styles.add} name="plus-circle" solid size={20} />
          </TouchableOpacity>
          </View>
          <View style={styles.map}>
            <MapExample />
          </View>


          <View style={styles.shiftDisplay}>

          <Text style={styles.shiftLabel}> MORNING </Text>
            <TouchableOpacity onPress= {() => NavigationService.navigate(
              'ToursList', {payload: this.state.items})
           }>
              <Text style={styles.addtour}> Add tour </Text>
            </TouchableOpacity>

          </View>


            <DragSortableView
              dataSource={this.state.selectedMorningTours}
              parentWidth={parentWidth}
              childrenWidth= {childrenWidth}
              childrenHeight={childrenHeight}
              scaleStatus={'scaleY'}
              onDragStart={(startIndex,endIndex)=>{
                  this.setState({
                      scrollEnabled: false
                  })
              }}
              onDragEnd={(startIndex)=>{
                  this.setState({
                      scrollEnabled: true
                  })
              }}
              onDataChange = {(data)=>{
                  if (data.length != this.state.data.length) {
                      this.setState({
                          data: data
                      })
                  }
              }}
              keyExtractor={(item,index)=> item.name} // FlatList作用一样，优化
              onClickItem={(data,item,index)=>{

              }}
              renderItem={(item,index)=>{
                  return this.renderItem(item,index)
              }}
              />


          <Text style={styles.shiftLabel}> AFTERNOON </Text>


                      <DragSortableView
                        dataSource={this.state.selectedAfternoonTours}
                        parentWidth={parentWidth}
                        childrenWidth= {childrenWidth}
                        childrenHeight={childrenHeight}
                        scaleStatus={'scaleY'}
                        onDragStart={(startIndex,endIndex)=>{
                            this.setState({
                                scrollEnabled: false
                            })
                        }}
                        onDragEnd={(startIndex)=>{
                            this.setState({
                                scrollEnabled: true
                            })
                        }}
                        onDataChange = {(data)=>{
                            if (data.length != this.state.data.length) {
                                this.setState({
                                    data: data
                                })
                            }
                        }}
                        keyExtractor={(item,index)=> item.name} // FlatList作用一样，优化
                        onClickItem={(data,item,index)=>{

                        }}
                        renderItem={(item,index)=>{
                            return this.renderItem(item,index)
                        }}
                        />

          <Text style={styles.shiftLabel}> NIGHT </Text>

              <DragSortableView
                  dataSource={this.state.selectedNightTours}

                  parentWidth={parentWidth}

                  childrenWidth= {childrenWidth}
                  childrenHeight={childrenHeight}

                  scaleStatus={'scaleY'}

                  onDragStart={(startIndex,endIndex)=>{
                      this.setState({
                          scrollEnabled: false
                      })
                  }}
                  onDragEnd={(startIndex)=>{
                      this.setState({
                          scrollEnabled: true
                      })
                  }}
                  onDataChange = {(data)=>{
                      if (data.length != this.state.data.length) {
                          this.setState({
                              data: data
                          })
                      }
                  }}
                  keyExtractor={(item,index)=> item.name} // FlatList作用一样，优化
                  onClickItem={(data,item,index)=>{

                  }}
                  renderItem={(item,index)=>{
                      return this.renderItem(item,index)
                  }}
              />
        </ScrollView>
      )
  }

  renderItem(item,index) {
      return (
              <View style={styles.item_children}>
                  <Image
                    style={styles.item_icon}
                    source={item.icon}
                  />
                  <Text style={styles.item_text}>{item.name}</Text>

                  <TouchableOpacity style={styles.deleteButton}>
                    <FontAwesome5 onPress={() =>
                      this.removeTour(item)
                    }
                      name="times-circle" solid size={20} />
                  </TouchableOpacity>
              </View>
      )
  }



}

export default withNavigationFocus(NewDragableExample)

const styles = StyleSheet.create({
mainContainer: {
      flex: 1,
      backgroundColor: 'lightgray',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: 'rgba(255,255,255,0.1)',


  },
  gridView: {
    marginTop: 10,
    padding: 10,
  },
  trash: {
    padding: 20,
    color: 'white',

  },
  add: {
    padding: 20,
    color: 'white',
  },

  item: {
      width: childrenWidth,
      height: childrenHeight,
      justifyContent: 'center',
      alignContent: 'space-around'

  },
  item_children: {
      marginLeft: 10,
      marginRight: 10,
      width: childrenWidth,
      height: childrenHeight-40,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: 4,
      backgroundColor: 'white',
      padding: 20
    },
  item_icon: {
      width: childrenHeight*0.3,
      height: childrenHeight*0.3,
      resizeMode: 'contain',
  },
  item_text: {
      color: '#2ecc71'
  },
  deleteButton: {
    marginBottom: 90,
    marginRight: -26,
  },
  shiftLabel: {
    //backgroundColor:,
    padding: 15,
    paddingVertical: 20,
    marginLeft: 5
  },
  daysButton: {
    backgroundColor: 'grey',
    padding: 10
  },
  map: {
    minHeight: 200,
    minWidth: 20,
  },
  shiftDisplay: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  addtour: {
    padding: 5,
    margin:20,
    backgroundColor: 'blue',
    borderRadius: 20
  }
})
