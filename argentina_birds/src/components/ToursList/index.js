import React from 'react';
import { Image, Text, ScrollView, TouchableOpacity, StyleSheet  } from 'react-native';
import { List, ListItem } from 'react-native-elements'
import NavigationService from '../../navigation/NavigationService'

class ToursList extends React.Component {

  alertItemName = (item) => {
      alert(item.name)
   }


  render () {
    const {navigation} = this.props;
    const items = navigation.state.params.payload.tours
    const items_with_key = {"name":items}
    return (
      <ScrollView>

              {
                 items_with_key.name.map((item, index) => (
                    <TouchableOpacity
                       key = {item.name}
                       style = {styles.container}
                       onPress= {() => NavigationService.navigate(
                         'CreateItinerary2', {newTour: item})} 
                       >
                       <Text style = {styles.text}>
                          {item.name}
                       </Text>
                       <Image
                       source={{uri:item.photo}}
                       style={{width: 100, height: 100}}
                       />
                    </TouchableOpacity>
                 ))
              }


      </ScrollView>

    );
  }
}

export default ToursList;

const styles = StyleSheet.create ({
   container: {
      padding: 10,
      marginTop: 3,

      alignItems: 'center',
   },
   text: {
      color: '#4f603c'
   }
})
