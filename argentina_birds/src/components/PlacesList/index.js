import React from 'react';
import { SafeAreaView, View, TouchableOpacity, Alert, StyleSheet, Text, ImageBackground } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';

import { connect } from 'react-redux';
import { getPlaces } from '../../actions/places';
import NavigationService from '../../navigation/NavigationService'

class PlacesList extends React.Component  {

  componentDidMount = () => {
    const { loadPlaces } = this.props;
    loadPlaces();
  }

  _onPressButton = (item) => {
    NavigationService.navigate('PlacesDetail', {item} );
  }

  render () {
    const { data } = this.props;

    return (
      <FlatGrid
        itemDimension={130}
        items={data}
        style={styles.gridView}
        // staticDimension={300}
        // fixed
        // spacing={20}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => this._onPressButton(item)}>
            <ImageBackground source={{ uri: item.photo}} style={{width: '100%', height: 150, justifyContent: 'flex-end',
                borderRadius: 5}}>
                <LinearGradient colors={["#ffffff00", "black"]} style={styles.linearGradient}>

              <Text style={styles.itemName}>{item.title}</Text>
              </LinearGradient>

            </ImageBackground>
            </TouchableOpacity>
        )}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  data: state.places.list,
});

const mapDispatchToProps = (dispatch) => ({
  loadPlaces: () => dispatch(getPlaces()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlacesList);


const styles = StyleSheet.create({
  gridView: {
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
    marginBottom: 20,
    position: 'absolute',
    bottom: 0,
    marginHorizontal: 16,
  },
  linearGradient: {
    flex: 1,
    justifyContent: 'space-between',
  },
});
