import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet } from 'react-native';
import MultiSelect from 'react-native-multiple-select';

export default class Selector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: [],
      //selectedValue: this.props.options[0].name,
    };
    //this.props.setFieldValue(this.props.options[0].name, this.state.selectedValue);

  }

  updateValue = async (itemValue) => {
    await this.setState({ selectedItems: itemValue });
    this.props.setFieldValue(this.props.label, this.state.selectedItems);
    console.warn(this.state.selectedItems);
  };

  render() {
    const { selectedItems } = this.state;
    const { items, label, setFieldValue } = this.props;

    return (
        <View>
          <Text style={{ paddingVertical: 10, paddingHorizontal: 10, fontSize: 16}}>{label}</Text>
          <View style={{ flex: 1, paddingHorizontal: 10}}>
            <MultiSelect
              items={items}
              uniqueKey="id"
              ref={component => {
                this.multiSelect = component;
              }}
              onSelectedItemsChange={this.updateValue}
              selectedItems={selectedItems}
              selectText="Selecciona la indicada"
              searchInputPlaceholderText="Ingresa la opcion..."
              tagRemoveIconColor="#CCC"
              tagBorderColor="#CCC"
              tagTextColor="#CCC"
              selectedItemTextColor="#CCC"
              selectedItemIconColor="#CCC"
              itemTextColor="#000"
              displayKey="name"
              searchInputStyle={{ color: '#CCC' }}
              submitButtonColor="#48d22b"
              submitButtonText="Guardar"
            />
          </View>
        </View>
    );
  }
}
